# Supplemental Material

## Invertible Non-Linear Layers for Symmetric Cryptographic Permutations over F_p^n targeting MPC/FHE/ZK Applications.

### by Lorenzo Grassi, Silvia Onofri, Marco Pedicini, Luca Sozzi.

This repository contains two directories:

1) [experiments:](https://gitlab.com/pis147879/supplemental-material/-/tree/main/experiments) a suite of running examples 
2) [proofs:](https://gitlab.com/pis147879/supplemental-material/-/tree/main/proofs) verification by computer algebra system Wolfram Mathematica that (some) collisions found are correct. 

