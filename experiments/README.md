This repository contains code and data used in the publication:

...omissis...  Invertible Non-Linear Layers for Symmetric Cryptographic Permutations over F_p^n targeting MPC/FHE/ZK Applications.

The code is developed in C++ and it is based on the gmp library.

To compile the code we suggest to create a build directory
and then compile and execute from there:

mkdir build
cd build
make -f ../Makefile speed

In general,  we have to run step 1 to obtain a list of balance (non-linear) functions
from which to read back and test invertibility. This can be done by adding the option "--reload" and 
specifying the number of output variables with an option "n=3" or "n=5" etc.

To compute balance functions, having the executable "balance-reload" we give the following at command line : 

./balance-reload 7 d=2 m=3  > test.log

after, running it with option --reload, it is possible to look for collisions at various n of the balance functions listed in the file:

./balance-reload 7 d=2 m=3 n=3  --reload test.log
./balance-reload 7 d=2 m=3 n=4  --reload test.log
./balance-reload 7 d=2 m=3 n=5  --reload test.log
./balance-reload 7 d=2 m=3 n=7  --reload test.log

As an alternative we provide some cases of use in the Makefile, for instance:

make -f ../Makefile -e  MM=3 NVAL=3 DD=2 PRIME=41 CD="" STARTCODE=0 ENDCODE=100 TOTALE=1 interval > test.log

(tests balance functions from code 0 to 100 with d=2 and m=3 with the prime p=41)

make -f ../Makefile -e MM=2  DD=2 PRIME=7 run  (to run experiments for d=2 and m=2)
make -f ../Makefile -e MM=3  DD=2 PRIME=7 run   (to run experiments for d=2 and m=3)
make -f ../Makefile -e MM=2  DD=3 PRIME=7 run   (to run experiments for d=3 and m=2)

we give in the build directory of the repository, files containing precomputed balance functions and collisions.


In the reload mode, the software performs the  "step 2" of the Algorithm on the list of functions contained in the file, and searches a collision for the $\mathcal  S$ function. If after exploring the whole domain, the collision is not found, then $\mathcal  S$ is a permutation. If the collision is found it is dumped.

./balance-reload 3 d=2 m=2 n=3 --reload file.txt

where the first argument "3" is the prime p, and here we look for collisions for the $\mathcal S$.

We provide a way to cut the run in a number of chunks that can be executed in parallel on muticore machines. The advantage is twofold: we reduce wallclock runtime by using multiple cores we may split tasks in small pieces. 

make -f ../Makefile -e  DD=2 MM=2 PRIME=3 CD=15 TOTALE=8 balance


