repo="builds"

MakeStat[p_, d_, m_, workdir_, bonly_, ntab_] := (
  ps = ToString[p];
  ctab = Take[{3, 4, 5, 7, 11, 13, 17, 23, 29, 31}, ntab];
  fdomain = If[((d == 3) && (m == 2)), 4 p^5,
                If[((d == 2) && (m == 3)), 4 p^7,
                 If[((d == 2) && (m == 2)), 4 p^3]]];
  logdir =
   workdir <> "/build-p" <> ps <> "d" <> ToString[d] <> "m" <>
    ToString[m] <> "/";
  Print[logdir, "\nfdomain=", fdomain];
  log = ReadString[logdir <> "balance.log"];
  btime = LOGSUM[p, log];
  If[bonly,
   Print[btime, "\nt1=", t1 = btime[[1]]/3600, " hours"];
   ,
   table = Map[
     (
       collisionlog =
        ReadString[logdir <> "runtime" <> ToString[#] <> ".log"];
       Prepend[LOGSUMCOL[p, collisionlog],
        " p=" <> ps <> " d=" <> ToString[d] <> " m=" <> ToString[m] <>
          " n=" <> ToString[#]]
       ) &, ctab];
   
   Export[logdir <> "distribution" <> ps <> ".pdf", TableForm@table,
    "PDF"];
   Print[btime, "\nt1=", t1 = btime[[1]]/3600, " t2=",
    t2 = (Plus @@ Map[#[[2]] &, table])/3600, " --> t1+t2=",
    t1 + t2];
   TableForm@table
   ])
   
   
MakeStat[5, 2, 2, repo, False, 4]
MakeStat[7, 2, 2, repo, False, 4]
MakeStat[11, 2, 2, repo, False, 3]
MakeStat[13, 2, 2, repo, True, 4]
MakeStat[17, 2, 2, repo, False, 3]
MakeStat[19, 2, 2, repo, False, 3]
MakeStat[23, 2, 2, repo, False, 3]


(*MakeStat[7,3,2,dgx,True,1]
MakeStat[13,3,2,dgx,False,3]
MakeStat[19,3,2,dgx,True,1]
MakeStat[31,3,2,laptop,True,1]

*)

MakeStat[7, 3, 2, repo, True, 1]
MakeStat[13, 3, 2, repo, False, 3]
MakeStat[19, 3, 2, repo, False, 1]
MakeStat[31, 3, 2, laptop, False, 2]


(*
MakeStat[3,2,3,laptop,False,6]
MakeStat[5,2,3,dgx,False,4]
MakeStat[7,2,3,laptop,False,3]
MakeStat[11,2,3,dgx,False,3]*)

MakeStat[3, 2, 3, repo, False, 6]
MakeStat[5, 2, 3, repo, False, 4]
MakeStat[7, 2, 3, repo, False, 3]
MakeStat[11, 2, 3, repo, False, 3]
