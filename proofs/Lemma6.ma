ClearAll[a200, x0, a020, x1 , a002, x2, a110, x0 ,x1 , a101, x0, x2 , a011, x1, x2,a100, x0 , a010, x1 ,a001 ,x2,s1,s2,s3,s4,s5,d1,d2,d3,d4,d5]


(*Definition for F in Theorem 3*)

F[x0_, x1_, x2_] := a200 x0^2 + a020 x1^2 + a002 x2^2 + a110 x0 x1 + a101 x0 x2 + a011 x1 x2 + a100 x0 + a010 x1 + a001 x2;

(*Definition for S_F in Theorem 3 for n=5*)

S[x0_, x1_, x2_, x3_, x4_] := {F[x0, x1, x2], F[x1, x2, x3], F[x2, x3, x4], F[x3, x4, x0], F[x4, x0, x1]};

(*Conditions*)
a101 = 0;
a200 = 0;
a020 = 0;
a002 = 0;
a011 = -a110;

(*Variables*)
z= -(a001/a011);
x2= x;
x3= y - a010/a011;
x4= a100/a011;
y2= y;
y3= x - a010/a011;

Print["z = ", z, "\nx2 = ", x2, "\nx3 = ", x3,"\nx4 = ", x4,"\ny2 = ", y2,"\ny3 = ", y3];

res1 = S[-(a001/a011), -(a001/a011), x, y - a010/a011, a100/a011];
res2 = S[-(a001/a011), -(a001/a011), y, x - a010/a011, a100/a011];

Print["Test collision S(z, z, x2, x3, x4) == S(z, z, y2, y3, x4): ", FullSimplify[res1-res2=={0,0,0,0,0}]];
