(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.3' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[     16169,        449]
NotebookOptionsPosition[     14059,        405]
NotebookOutlinePosition[     14515,        423]
CellTagsIndexPosition[     14472,        420]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[TextData[StyleBox["Computation for Section 6.1, Lemma 3, Case: \
a002,a200,a020!=0 (on page 20)", "Subsubsection"]], "Text",
 CellChangeTimes->{{3.869645719714031*^9, 3.869645738139091*^9}, {
   3.869645809108254*^9, 3.869645891366337*^9}, {3.8696459638252296`*^9, 
   3.869645987741007*^9}, {3.8696573111648893`*^9, 3.869657339824483*^9}, {
   3.869713897472868*^9, 3.869713898091695*^9}, {3.869743198620976*^9, 
   3.869743204649992*^9}, 3.869743256553279*^9, {3.8697443373086643`*^9, 
   3.86974433924326*^9}, {3.86974894842023*^9, 3.869748948657794*^9}, {
   3.869749004211622*^9, 
   3.86974900450344*^9}},ExpressionUUID->"9e0a9902-8de8-41d4-a685-\
a62dd569d5d3"],

Cell[CellGroupData[{

Cell[BoxData[{
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{"Get", "[", "\"\<Lemma3.ma\>\"", "]"}]}], "Input",
 CellChangeTimes->{{3.8696459845508423`*^9, 3.869646113958645*^9}, 
   3.8696807853717833`*^9, 3.869680873554049*^9, {3.86968095964962*^9, 
   3.86968098547611*^9}, 3.8696810366277018`*^9, {3.8697431777648687`*^9, 
   3.8697432227180653`*^9}, 3.8697443654724817`*^9},
 CellLabel->"In[1]:=",ExpressionUUID->"39a99273-db84-4e66-aa24-ebddab24aa86"],

Cell[CellGroupData[{

Cell[BoxData["\<\"Searching values for d_i=x_i-y_i and s_i=x_i+y_i\"\>"], \
"Print",
 CellChangeTimes->{
  3.869743223640654*^9, 3.869743492354783*^9, 3.869743561242482*^9, 
   3.86974419915868*^9, 3.869744243749977*^9, {3.869744366459012*^9, 
   3.8697443893493557`*^9}, 3.8697491821662083`*^9},
 CellLabel->
  "During evaluation of \
In[1]:=",ExpressionUUID->"bf280963-3450-44aa-bb60-12ba5f8d8430"],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"s3", "\[Rule]", 
       RowBox[{"-", 
        FractionBox["a010", "a020"]}]}]},
     {
      RowBox[{"d3", "\[Rule]", 
       FractionBox[
        RowBox[{"a020", " ", "d2"}], "a002"]}]},
     {
      RowBox[{"d4", "\[Rule]", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{
          RowBox[{"(", 
           RowBox[{"a002", "+", "a020"}], ")"}], " ", "d2"}], "a002"]}]}]}
    },
    GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Print",
 CellChangeTimes->{
  3.869743223640654*^9, 3.869743492354783*^9, 3.869743561242482*^9, 
   3.86974419915868*^9, 3.869744243749977*^9, {3.869744366459012*^9, 
   3.8697443893493557`*^9}, 3.869749182176126*^9},
 CellLabel->
  "During evaluation of \
In[1]:=",ExpressionUUID->"b60967e1-b2b2-4253-a082-5b0f4bf1653e"],

Cell[BoxData[
 TagBox[
  TagBox[GridBox[{
     {
      RowBox[{"d2", "\[Rule]", 
       RowBox[{
        FractionBox["a001", "a002"], "+", 
        RowBox[{"2", " ", "x2"}]}]}]},
     {
      RowBox[{"y2", "\[Rule]", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{"a001", "+", 
          RowBox[{"a002", " ", "x2"}]}], "a002"]}]}]},
     {
      RowBox[{"x3", "\[Rule]", 
       RowBox[{
        RowBox[{"-", 
         FractionBox["a010", 
          RowBox[{"2", " ", "a020"}]]}], "+", 
        FractionBox[
         RowBox[{"a020", " ", 
          RowBox[{"(", 
           RowBox[{"a001", "+", 
            RowBox[{"2", " ", "a002", " ", "x2"}]}], ")"}]}], 
         RowBox[{"2", " ", 
          SuperscriptBox["a002", "2"]}]]}]}]},
     {
      RowBox[{"y3", "\[Rule]", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{
          RowBox[{
           SuperscriptBox["a002", "2"], " ", "a010"}], "+", 
          RowBox[{"a001", " ", 
           SuperscriptBox["a020", "2"]}], "+", 
          RowBox[{"2", " ", "a002", " ", 
           SuperscriptBox["a020", "2"], " ", "x2"}]}], 
         RowBox[{"2", " ", 
          SuperscriptBox["a002", "2"], " ", "a020"}]]}]}]},
     {
      RowBox[{"x4", "\[Rule]", 
       RowBox[{
        RowBox[{"-", 
         FractionBox["a100", 
          RowBox[{"2", " ", "a200"}]]}], "+", 
        FractionBox[
         RowBox[{"a200", " ", 
          RowBox[{"(", 
           RowBox[{"a001", "+", 
            RowBox[{"2", " ", "a002", " ", "x2"}]}], ")"}]}], 
         RowBox[{"2", " ", 
          SuperscriptBox["a002", "2"]}]]}]}]},
     {
      RowBox[{"y4", "\[Rule]", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{
          RowBox[{
           SuperscriptBox["a002", "2"], " ", "a100"}], "+", 
          RowBox[{"a001", " ", 
           SuperscriptBox["a200", "2"]}], "+", 
          RowBox[{"2", " ", "a002", " ", 
           SuperscriptBox["a200", "2"], " ", "x2"}]}], 
         RowBox[{"2", " ", 
          SuperscriptBox["a002", "2"], " ", "a200"}]]}]}]}
    },
    GridBoxAlignment->{"Columns" -> {{Left}}, "Rows" -> {{Baseline}}},
    GridBoxSpacings->{"Columns" -> {
        Offset[0.27999999999999997`], {
         Offset[0.5599999999999999]}, 
        Offset[0.27999999999999997`]}, "Rows" -> {
        Offset[0.2], {
         Offset[0.4]}, 
        Offset[0.2]}}],
   Column],
  Function[BoxForm`e$, 
   TableForm[BoxForm`e$]]]], "Print",
 CellChangeTimes->{
  3.869743223640654*^9, 3.869743492354783*^9, 3.869743561242482*^9, 
   3.86974419915868*^9, 3.869744243749977*^9, {3.869744366459012*^9, 
   3.8697443893493557`*^9}, 3.8697491823515673`*^9},
 CellLabel->
  "During evaluation of \
In[1]:=",ExpressionUUID->"7280cfb4-0e15-412f-bd6f-8e8050ba77cf"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Test collision S(0,0,x2,x3,x4) == S(0,0,y2,y3,y4): \"\>", 
   "\[InvisibleSpace]", "True"}],
  SequenceForm["Test collision S(0,0,x2,x3,x4) == S(0,0,y2,y3,y4): ", True],
  Editable->False]], "Print",
 CellChangeTimes->{
  3.869743223640654*^9, 3.869743492354783*^9, 3.869743561242482*^9, 
   3.86974419915868*^9, 3.869744243749977*^9, {3.869744366459012*^9, 
   3.8697443893493557`*^9}, 3.8697491823570147`*^9},
 CellLabel->
  "During evaluation of \
In[1]:=",ExpressionUUID->"6fb2beb8-a33c-4b40-b65f-4c26879aef51"]
}, Open  ]]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.869646621614717*^9, 3.8696466716440287`*^9}, 
   3.86964676882953*^9},ExpressionUUID->"8b5b2b99-ff2b-4bc6-be9f-\
ddaa8d53d0c9"],

Cell[BoxData[
 StyleBox[
  RowBox[{
   RowBox[{"Collision", " ", "for", " ", "Section", " ", "6.3"}], ",", " ", 
   RowBox[{"Lemma", " ", "5"}], ",", " ", 
   RowBox[{
    RowBox[{"Case", ":", " ", "a110"}], " ", "=", " ", 
    RowBox[{
     RowBox[{"0", " ", 
      RowBox[{"and", "/", "or"}], " ", "a011"}], " ", "=", " ", "0"}]}], ",", 
   " ", 
   RowBox[{
    RowBox[{"Subcase", ":", " ", "a020"}], "=", "0"}], ",", " ", 
   RowBox[{
    RowBox[{"SubSubCase", ":", " ", "a020"}], "=", "0"}], ",", 
   RowBox[{"a002", "=", 
    RowBox[{
     RowBox[{"a200", " ", "and", " ", "a001"}], "=", 
     RowBox[{"a100", ".", " ", 
      RowBox[{"(", 
       RowBox[{"on", " ", "page", " ", "25"}], ")"}]}]}]}]}], 
  "Subsubsection"]], "Input",
 CellChangeTimes->{{3.869743263490727*^9, 3.869743276820797*^9}, {
  3.869748951978643*^9, 3.869748955160522*^9}, {3.869748992082788*^9, 
  3.869749000903841*^9}},ExpressionUUID->"150ee3fe-509d-427b-9ddd-\
0f48f06daa7a"],

Cell[" ", "Text",
 Editable->False,
 Selectable->False,
 CellFrame->{{0, 0}, {0, 0.5}},
 ShowCellBracket->False,
 CellMargins->{{0, 0}, {1, 1}},
 CellElementSpacings->{"CellMinHeight"->1},
 CellFrameMargins->0,
 CellFrameColor->RGBColor[0, 0, 1],
 CellSize->{
  Inherited, 3},ExpressionUUID->"994e0e6f-12f3-41f7-9c71-f2b96f02156c"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Get", "[", "\"\<Lemma5.ma\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.869743285304843*^9, 3.869743285460793*^9}},
 CellLabel->"In[3]:=",ExpressionUUID->"78228405-73d9-4aaf-99f2-ebf372ad2b0e"],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"x2 = \"\>", "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", 
     FractionBox["a100", 
      RowBox[{"2", " ", "a200"}]]}], "+", 
    FractionBox["x", "2"]}], "\[InvisibleSpace]", "\<\"\\nx3 = \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{
    FractionBox[
     RowBox[{"1", "-", 
      RowBox[{"2", " ", 
       SuperscriptBox["a100", "2"]}]}], 
     RowBox[{"4", " ", "a100", " ", "a200"}]], "-", 
    RowBox[{"a100", " ", "x"}]}], "\[InvisibleSpace]", "\<\"\\nx4 = \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", 
     FractionBox["a100", 
      RowBox[{"2", " ", "a200"}]]}], "+", 
    FractionBox["x", "2"]}], "\[InvisibleSpace]", "\<\"\\ny2 = \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", 
     FractionBox["a100", 
      RowBox[{"2", " ", "a200"}]]}], "-", 
    FractionBox["x", "2"]}], "\[InvisibleSpace]", "\<\"\\ny3 = \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{
    FractionBox[
     RowBox[{"1", "-", 
      RowBox[{"2", " ", 
       SuperscriptBox["a100", "2"]}]}], 
     RowBox[{"4", " ", "a100", " ", "a200"}]], "+", 
    RowBox[{"a100", " ", "x"}]}], "\[InvisibleSpace]", "\<\"\\ny4 = \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{
    RowBox[{"-", 
     FractionBox["a100", 
      RowBox[{"2", " ", "a200"}]]}], "-", 
    FractionBox["x", "2"]}]}],
  SequenceForm[
  "x2 = ", Rational[-1, 2] $CellContext`a100/$CellContext`a200 + 
   Rational[1, 2] $CellContext`x, "\nx3 = ", 
   Rational[1, 4] $CellContext`a100^(-1) (1 - 
     2 $CellContext`a100^2)/$CellContext`a200 - $CellContext`a100 \
$CellContext`x, "\nx4 = ", 
   Rational[-1, 2] $CellContext`a100/$CellContext`a200 + 
   Rational[1, 2] $CellContext`x, "\ny2 = ", 
   Rational[-1, 2] $CellContext`a100/$CellContext`a200 + 
   Rational[-1, 2] $CellContext`x, "\ny3 = ", 
   Rational[1, 4] $CellContext`a100^(-1) (1 - 
     2 $CellContext`a100^2)/$CellContext`a200 + $CellContext`a100 \
$CellContext`x, "\ny4 = ", 
   Rational[-1, 2] $CellContext`a100/$CellContext`a200 + 
   Rational[-1, 2] $CellContext`x],
  Editable->False]], "Print",
 CellChangeTimes->{{3.869743280529627*^9, 3.869743286229045*^9}, 
   3.8697442845867863`*^9, 3.86974439403655*^9, 3.869749186529283*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"a3d5b380-deee-4b73-bc98-dc8d55ef3538"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Test collision  S(0,0,x2,x3,x4) == S(0,0,y2,y3,y4): \"\>", 
   "\[InvisibleSpace]", "True"}],
  SequenceForm["Test collision  S(0,0,x2,x3,x4) == S(0,0,y2,y3,y4): ", True],
  Editable->False]], "Print",
 CellChangeTimes->{{3.869743280529627*^9, 3.869743286229045*^9}, 
   3.8697442845867863`*^9, 3.86974439403655*^9, 3.869749186531691*^9},
 CellLabel->
  "During evaluation of \
In[3]:=",ExpressionUUID->"2511e6ea-22b5-43ad-a6ea-4031e151dab1"]
}, Open  ]]
}, Open  ]],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.8697432884808292`*^9, 
  3.869743290343545*^9}},ExpressionUUID->"a285b021-3fb4-42c9-84c5-\
e7a6887064e5"],

Cell[BoxData[
 StyleBox[
  RowBox[{
   RowBox[{"Collision", " ", "for", " ", "Section", " ", "6.4"}], ",", " ", 
   RowBox[{"Lemma", " ", "6"}], ",", " ", 
   RowBox[{"Case", ":", " ", "a011"}], ",", 
   RowBox[{"a110", "!=", "0"}], ",", " ", 
   RowBox[{
    RowBox[{"Subcase", ":", " ", "a200"}], "=", 
    RowBox[{"a020", "=", 
     RowBox[{"a002", "=", 
      RowBox[{"0", " ", 
       RowBox[{"(", 
        RowBox[{"on", " ", "page", " ", "30"}], ")"}]}]}]}]}]}], 
  "Subsubsection"]], "Input",
 CellChangeTimes->{{3.869743328264089*^9, 3.869743336659584*^9}, {
  3.869749009612768*^9, 
  3.869749034170433*^9}},ExpressionUUID->"383cc9a7-fd09-41d7-af77-\
4b7e16a17f9a"],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Get", "[", "\"\<Lemma6.ma\>\"", "]"}]], "Input",
 CellChangeTimes->{{3.8697433388198843`*^9, 3.8697433464230537`*^9}},
 CellLabel->"In[4]:=",ExpressionUUID->"4848dd30-574a-46d5-9d01-8fcc3d8b699c"],

Cell[CellGroupData[{

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"z = \"\>", "\[InvisibleSpace]", 
   FractionBox["a001", "a110"], "\[InvisibleSpace]", "\<\"\\nx2 = \"\>", 
   "\[InvisibleSpace]", "x", "\[InvisibleSpace]", "\<\"\\nx3 = \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{
    FractionBox["a010", "a110"], "+", "y"}], 
   "\[InvisibleSpace]", "\<\"\\nx4 = \"\>", "\[InvisibleSpace]", 
   RowBox[{"-", 
    FractionBox["a100", "a110"]}], "\[InvisibleSpace]", "\<\"\\ny2 = \"\>", 
   "\[InvisibleSpace]", "y", "\[InvisibleSpace]", "\<\"\\ny3 = \"\>", 
   "\[InvisibleSpace]", 
   RowBox[{
    FractionBox["a010", "a110"], "+", "x"}]}],
  SequenceForm[
  "z = ", $CellContext`a001/$CellContext`a110, "\nx2 = ", $CellContext`x, 
   "\nx3 = ", $CellContext`a010/$CellContext`a110 + $CellContext`y, 
   "\nx4 = ", -$CellContext`a100/$CellContext`a110, "\ny2 = ", $CellContext`y,
    "\ny3 = ", $CellContext`a010/$CellContext`a110 + $CellContext`x],
  Editable->False]], "Print",
 CellChangeTimes->{3.869744304250401*^9, 3.869744398574851*^9, 
  3.869749199346444*^9},
 CellLabel->
  "During evaluation of \
In[4]:=",ExpressionUUID->"559603f6-459c-4cbe-aa2b-37e265c19df9"],

Cell[BoxData[
 InterpretationBox[
  RowBox[{"\<\"Test collision S(z, z, x2, x3, x4) == S(z, z, y2, y3, x4): \
\"\>", "\[InvisibleSpace]", "True"}],
  SequenceForm[
  "Test collision S(z, z, x2, x3, x4) == S(z, z, y2, y3, x4): ", True],
  Editable->False]], "Print",
 CellChangeTimes->{3.869744304250401*^9, 3.869744398574851*^9, 
  3.86974919934864*^9},
 CellLabel->
  "During evaluation of \
In[4]:=",ExpressionUUID->"a3396d8a-6ab6-4bbe-ab97-05eed1d03a66"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1350, 755},
WindowMargins->{{-1686, Automatic}, {Automatic, -545}},
PrintingCopies->1,
PrintingPageRange->{1, Automatic},
FrontEndVersion->"13.0 for Mac OS X x86 (64-bit) (December 2, 2021)",
StyleDefinitions->"Default.nb",
ExpressionUUID->"85a4749a-3b7b-47db-8b5d-2bc388a3e0bf"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[558, 20, 673, 10, 40, "Text",ExpressionUUID->"9e0a9902-8de8-41d4-a685-a62dd569d5d3"],
Cell[CellGroupData[{
Cell[1256, 34, 547, 10, 52, "Input",ExpressionUUID->"39a99273-db84-4e66-aa24-ebddab24aa86"],
Cell[CellGroupData[{
Cell[1828, 48, 400, 8, 24, "Print",ExpressionUUID->"bf280963-3450-44aa-bb60-12ba5f8d8430"],
Cell[2231, 58, 1153, 36, 82, "Print",ExpressionUUID->"b60967e1-b2b2-4253-a082-5b0f4bf1653e"],
Cell[3387, 96, 2742, 84, 168, "Print",ExpressionUUID->"7280cfb4-0e15-412f-bd6f-8e8050ba77cf"],
Cell[6132, 182, 564, 12, 24, "Print",ExpressionUUID->"6fb2beb8-a33c-4b40-b65f-4c26879aef51"]
}, Open  ]]
}, Open  ]],
Cell[6723, 198, 176, 3, 30, "Input",ExpressionUUID->"8b5b2b99-ff2b-4bc6-be9f-ddaa8d53d0c9"],
Cell[6902, 203, 960, 25, 70, "Input",ExpressionUUID->"150ee3fe-509d-427b-9ddd-0f48f06daa7a"],
Cell[7865, 230, 331, 10, 6, "Text",ExpressionUUID->"994e0e6f-12f3-41f7-9c71-f2b96f02156c"],
Cell[CellGroupData[{
Cell[8221, 244, 216, 3, 30, "Input",ExpressionUUID->"78228405-73d9-4aaf-99f2-ebf372ad2b0e"],
Cell[CellGroupData[{
Cell[8462, 251, 2331, 62, 230, "Print",ExpressionUUID->"a3d5b380-deee-4b73-bc98-dc8d55ef3538"],
Cell[10796, 315, 491, 10, 24, "Print",ExpressionUUID->"2511e6ea-22b5-43ad-a6ea-4031e151dab1"]
}, Open  ]]
}, Open  ]],
Cell[11314, 329, 154, 3, 30, "Input",ExpressionUUID->"a285b021-3fb4-42c9-84c5-e7a6887064e5"],
Cell[11471, 334, 674, 18, 37, "Input",ExpressionUUID->"383cc9a7-fd09-41d7-af77-4b7e16a17f9a"],
Cell[CellGroupData[{
Cell[12170, 356, 220, 3, 30, "Input",ExpressionUUID->"4848dd30-574a-46d5-9d01-8fcc3d8b699c"],
Cell[CellGroupData[{
Cell[12415, 363, 1156, 25, 188, "Print",ExpressionUUID->"559603f6-459c-4cbe-aa2b-37e265c19df9"],
Cell[13574, 390, 457, 11, 24, "Print",ExpressionUUID->"a3396d8a-6ab6-4bbe-ab97-05eed1d03a66"]
}, Open  ]]
}, Open  ]]
}
]
*)

