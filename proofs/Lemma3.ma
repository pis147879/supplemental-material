ClearAll[a200, x0, a020, x1 , a002, x2, a110, x0 ,x1 , a101, x0, x2 , a011, x1, x2,a100, x0 , a010, x1 ,a001 ,x2,s1,s2,s3,s4,s5,d1,d2,d3,d4,d5]

(*Definition for F in Theorem 3*)

F[x0_, x1_, x2_] := a200 x0^2 + a020 x1^2 + a002 x2^2 + a110 x0 x1 + a101 x0 x2 + a011 x1 x2 + a100 x0 + a010 x1 + a001 x2;

(*Definition for S_F in Theorem 3 for n=5*)

S[x0_, x1_, x2_, x3_, x4_] := {F[x0, x1, x2], F[x1, x2, x3], F[x2, x3, x4], F[x3, x4, x0], F[x4, x0, x1]};

(*Conditions*)
a200 = -a002 - a020;
a110 = 0;
a101 = 0; 
a011 = 0;

(*To find s3*)
Clear[s3];

a200 = -a002 - a020; 
sum=Solve[{(-a020 a001 + a010 a002)*(a200^2 s3 + a020 a100 + a200 (a100 - a010)) - (a020 a100 - a010 a200) (a002^2 s3 + a001 a002) == 0}, s3];

Print["Searching values for d_i=x_i-y_i and s_i=x_i+y_i"];

(*To find d3 and d4*)
Clear[d3, d4]; 

Solve[ d2 (-a020 a001 + a010 a002) + d3 (a002^2 (-a010/a020) + a001 a002) == 0, d3];

diff=Solve[
	{
		d4 == -d2 - d3, 
		d3 - (a020 /a002) d2 == 0
	}, {d3, d4}];
Print@TableForm@Join[sum[[1]],diff[[1]]];

(*Given the values for d_i and s_i, find x_i and y_i*)
Clear[a200];
d3 = (a020/a002) d2;
d4 = (a200/a002) d2;
s2 = -a001/a002;
s3 = -a010/a020; 
s4 = -a100/a200;
var=FullSimplify[
 ExpandAll[
  Solve[
  	{
  	 x2 + y2 - s2 == 0, 
  	 x2 - y2 - d2 == 0, 
  	 x3 + y3 - s3 == 0, 
     x3 - y3 - d3 == 0, 
     x4 + y4 - s4 == 0, 
     x4 - y4 - d4 == 0
    },
    {d2, y2, x3, y3, x4, y4}
   ]
  ]
 ];

Print@TableForm[var[[1]]];

(*Collision*)
res1 = S[0, 
         0, 
         x2, 
         -((a002^2 a010 - a001 a020^2 - 2 a002 a020^2 x2)/(2 a002^2 a020)), 
         -((a002^2 a100 - a001 a200^2 - 2 a002 a200^2 x2)/(2 a002^2 a200))
       ];
res2 = S[0, 
         0, 
         -((a001 + a002 x2)/a002), 
         -((a002^2 a010 + a001 a020^2 + 2 a002 a020^2 x2)/(2 a002^2 a020)), 
         -((a002^2 a100 + a001 a200^2 + 2 a002 a200^2 x2)/(2 a002^2 a200))
       ];

Print["Test collision S(0,0,x2,x3,x4) == S(0,0,y2,y3,y4): ", FullSimplify[res1-res2=={0,0,0,0,0}]];
